CPP=g++
CFLAGS = -c
CC= $(CPP) $(CFLAGS)

all: client.o
	$(CPP) -o client client.o
client.o: client.cc
	$(CC) client.cc
