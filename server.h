#pragma once

#include <cstdio>
#include <cmath>
#include <map>
#include <vector>
#define Z 4
using namespace std;

struct Bucket {
    pair<int, string> data[Z];
    int load;
    Bucket() {
        load = 0;
        for (int i = 0; i < Z; ++i)
            data[i] = make_pair(-1, "DUMMY");
    }

    void insert(int x, string y) {
        if (load == Z) printf("OVERLOAD IN BUCKET\n");
        data[load] = make_pair(x, y);
        load++;
    }
};

struct Server {
    int height, capacity;
    vector<Bucket> store;

    Server(int c) {
        height = ceil(log2((double)c)) + 1;
        capacity = 1 << (height - 1);
        init();
    }

    void init() {
        store.clear();
        store.push_back(Bucket());
        for (int i = 1; i <= (1 << height) - 1; ++i)
            store.push_back(Bucket());
    }

    map<int, string> fetchAlongPath(int x) {
        map<int, string> ans;
        x += (1 << (height - 1));
        while (x > 0) {
            Bucket tmp = store[x];
            for (int i = 0; i < tmp.load; ++i)
                ans[tmp.data[i].first] = tmp.data[i].second;
            x /= 2;
        }
        return ans;
    }

    void writeBucket(int pos, int l, Bucket res) {
        pos += (1 << (height - 1));
        for (int i = 0; i < l; ++i) pos /= 2;
        store[pos] = res;
    }
};
