#include <cstdio>
#include <string>
#include <map>
#include "server.h"
#include <ctime>
#include <cstdlib>
#include <iostream>
#define Z 4

using namespace std;

int rnd(int bound) {
    return ((rand()<<15) + rand()) % bound;
}

struct Client {
    Server* server;
    map<int, int> posMap;
    map<int, string> stash;
    int height, capacity;

    Client(Server* s) {
        server = s;
        posMap.clear();
        stash.clear();
        height = server->height;
        capacity = server->capacity;
        for (int i = 0; i < capacity; ++i)
            posMap[i] = rnd(capacity);
    }

    string read(int key) {
        string ans;
        int x = posMap[key];
        posMap[key] = rnd(capacity);

       // cout << x << endl;

        map<int, string> ret = server->fetchAlongPath(x);
        map<int, string>::iterator iter;
        for (iter = ret.begin(); iter != ret.end(); ++iter) {
            stash[iter->first] = iter->second;
        //    cout << iter->first << " --- " << iter->second << endl;
        }

        ans = stash[key];

        //printf("!!!!!!!!!!!!!!\n");

        for (int i = 0; i < height; ++i) {
            //printf("%d !!!!!!!!!!!!!!\n", i);
            int tot = 0;
            Bucket res;
            map<int, string>::iterator j, tmp;
            j = stash.begin();
            while (j != stash.end() && tot < Z) {
                if (check(posMap[j->first], x, i)) {
                    res.insert(j->first, j->second);
                    tmp = j; ++j; stash.erase(tmp);
                    ++tot;
                } else ++j;
            }
            server->writeBucket(x, i, res);
        }

        return ans;
    }

    void write(int key, string data) {
        int x = posMap[key];
        posMap[key] = rnd(capacity);

        map<int, string> ret = server->fetchAlongPath(x);
        map<int, string>::iterator iter;
        for (iter = ret.begin(); iter != ret.end(); ++iter)
            stash[iter->first] = iter->second;

        //map<int, string>::iterator it;
        stash[key] = data;
        //for (it = stash.begin(); it != stash.end(); ++it)
         //   cout << it->first << " --- " << it->second << endl;
        //cout << posMap[key] << endl;
        //cout << x << endl;

        for (int i = 0; i < height; ++i) {
            //printf("%d !!!!!!!!!!!!!!\n", i);
            int tot = 0;
            Bucket res;
            map<int, string>::iterator j, tmp;
            j = stash.begin();
            while (j != stash.end() && tot < Z) {
                //cout << posMap[j->first] << endl;
                if (check(posMap[j->first], x, i)) {
                    res.insert(j->first, j->second);
                    //cout << j->first << "----"  << j->second << endl;
                    tmp = j; ++j; stash.erase(tmp);
                    ++tot;

                } else ++j;
            }
            server->writeBucket(x, i, res);
        }
    }

    bool check(int x, int y, int l) {
        if (l == 0 && x == y) return true;
        int tmp = ((1 << height) - 1) ^ (1 << (l-1));
        return (x & tmp) == (y & tmp);
    }
};

int main() {
    srand(time(NULL));
    Server server(1000);
    Client client(&server);

    printf("%d\n", server.capacity);
    printf("%d\n", server.height);

    printf("Write start!\n");
    long t1 = clock();
    for (int tt = 0; tt < 1000; ++tt) {
        //printf("%d\n", tt);
        char tmp[1000];
        sprintf(tmp, "Message %d", tt);
        string ss(tmp);
        client.write(tt, ss);
    }
    long t2 = clock();
    printf("Write complete!\n");
    printf("Inserting lasts: %ldms\n", t2 - t1);

    //for (int i = 0; i < 1000; ++i)
    //    cout << client.read(i) << endl;

    return 0;
}
